# Name: Sharwari Phadnis
# Student ID: 012168884
# CMPE 285 [Python Stock Profit Calculator]

from collections import OrderedDict
from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__)

taxInfo = {}
stock = OrderedDict()

# index.html gets input required to calculate Stock Profit
@app.route('/')
def index():
    if request.method == 'GET':
        return render_template('index.html')

@app.route('/stock',methods = ['POST', 'GET'])
def calculate_stock():
    if request.method == 'POST':
        print(request.form)
        taxInfo["ticketSymbol"] = request.form['ticketSymbol']
        taxInfo["allotment"] = float(request.form['allotment'])
        taxInfo["finalSharePrice"] = float(request.form['finalSharePrice'])
        taxInfo["sellCommission"] = float(request.form['sellCommission'])
        taxInfo["initialSharePrice"] = float(request.form['initialSharePrice'])
        taxInfo["buyCommission"] = float(request.form['buyCommission'])
        taxInfo["capitalGainTaxRate"] = float(request.form['capitalGainTaxRate'])

        calculateTax()
        populateFinalReport()
        print(stock)
        return render_template('stockReport.html', stock=stock)


def calculateTax():
    taxInfo["proceeds"] = int(taxInfo["allotment"]) * taxInfo["finalSharePrice"]
    taxInfo["shareDiff"] = (taxInfo["finalSharePrice"] - taxInfo["initialSharePrice"])
    taxInfo["totalTax"] = (taxInfo["shareDiff"] * int(taxInfo["allotment"])) - taxInfo["buyCommission"] \
                          - taxInfo["sellCommission"]
    taxInfo["tax"] = taxInfo["totalTax"] * taxInfo["capitalGainTaxRate"] / 100
    taxInfo["initialTotal"] = int(taxInfo["allotment"]) * float(taxInfo["initialSharePrice"])
    taxInfo["totalCost"] = (int(taxInfo["initialTotal"])) + \
                           float(taxInfo["buyCommission"]) + float(taxInfo["sellCommission"]) + taxInfo["tax"]
    taxInfo["netProfit"] = taxInfo["proceeds"] - taxInfo["totalCost"]
    taxInfo["roi"] = taxInfo["netProfit"] / taxInfo["totalCost"] * 100
    taxInfo["breakEvenPrice"] = ((int(taxInfo["initialTotal"])) + float(taxInfo["buyCommission"]) + float(taxInfo["sellCommission"])) / int(taxInfo["allotment"])


def populateFinalReport():
    stock["Ticket Symbol:"] = taxInfo["ticketSymbol"]
    stock["Allotment:"] = taxInfo["allotment"]
    stock["Final Share Price:"] = taxInfo["finalSharePrice"]
    stock["Buy Commission:"] = str(taxInfo["buyCommission"])
    stock["Sell Commission:"] = str(taxInfo["sellCommission"])
    stock["Capital Gain Tax Rate (%):"] = taxInfo["capitalGainTaxRate"]

    stock["Proceeds:"] = "$%.2f" % (taxInfo["proceeds"])
    stock["Cost:"] = "$%.2f" % (taxInfo["totalCost"])
    stock["Total Purchase Price:"] = str(taxInfo["allotment"]) + \
        " x $" + str(taxInfo["initialSharePrice"]) + " = %.2f" % ((int(taxInfo["initialTotal"])))
    stock["Tax on Capital Gain:"] = str(taxInfo["capitalGainTaxRate"]) + "% of $" + "%.2f" % (taxInfo["totalTax"]) +\
        " = %.2f" % taxInfo["tax"]
    stock["Net profit:"] = "$" + "%.2f" % (taxInfo["netProfit"])
    stock["Return on Investment:"] = "%.2f" % (taxInfo["roi"]) + "%"
    stock["To break even, you should have a final share price of :"] = "$" + "%.2f" % (taxInfo["breakEvenPrice"])

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
